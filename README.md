다음의 순서대로 진행해주세요!


------------우분투 16.04 에서 진행---------------

apt-get update

apt-get -y upgrade

reboot

apt-get -y install python python-configparser

git clone https://gitlab.com/jj2928/masternode.git

cd masternode

########### 2개 이상의 ipv4 주소를 사용할 경우 add_1.py 혹은 add_2.py 를 반드시 진행해주세요.############

@@  VPS 서버에 ip 한개를 추가했을 경우

cd masternode

python add_1.py

@@  VPS 서버에 ip 한개를 추가했을 경우

cd masternode

python add_2.py

만약 ip 를 추가하지 않았다면 위의 과정은 pass 해도 됩니다.

#########################################################################################################

python masternode.py

-------------------------------------------------

위에 까지 진행하시면 아래와 같은 문구가 나옵니다.

What is the index number for the Masternode would you like to setup?
: 몇번째 마스터노드를 설치하시겠습니까? (1~3 사이를 입력해주세요.)

Input the new ipv4 address.
: 새로 추가할 ipv4 주소를 입력해주세요.

이 후, 출력된 ip:port GENKEY 값을 복사하고 본인의 컴퓨터에 저장된 Masternode.conf 파일에 옮겨주세요.

문의사항은 오픈카톡에서 해주세요. 
https://open.kakao.com/o/gZOtt9kb

Original source:
https://gitlab.com/minerscore/masternodes/